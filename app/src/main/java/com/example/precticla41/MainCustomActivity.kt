package com.example.precticla41

import android.app.AlertDialog
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.precticla41.model.Users
import com.example.precticla41.ui.theme.Precticla41Theme
import com.example.precticla41.viewModel.MainViewModel

class MainCustomActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Precticla41Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    color = MaterialTheme.colors.background
                ) {
                    UserList()
                }
            }
        }
    }
}


@Composable
fun UserList() {
    val mainViewModel = hiltViewModel<MainViewModel>()
    LaunchedEffect(key1 = "unit", block = {
        mainViewModel.getUserList()
    })
    val data = mainViewModel.userResponse.collectAsState()
    LazyColumn {
        itemsIndexed(data.value) { _, item ->
            DataListView(user = item, viewModel = mainViewModel)
        }
    }
}


@OptIn(ExperimentalFoundationApi::class)
@Composable
fun DataListView(user: Users, viewModel: MainViewModel) {
    val context = LocalContext.current
    Card(
        modifier = Modifier
            .padding(8.dp, 4.dp)
            .fillMaxWidth()
            .combinedClickable(
                onClick = {
                },
                onLongClick = {
                    val builder = AlertDialog.Builder(context)
                    builder
                        .setMessage(
                            "Do you want to remove " + user.name
                                    + " from list?"
                        )
                        .setCancelable(true)
                        .setPositiveButton("Yes") { _, _ ->
                            viewModel.removeItem(user)
                        }
                        .setNegativeButton(
                            "No"
                        ) { _, _ -> }
                    val alertDialog = builder.create()
                    alertDialog.setTitle("")
                    alertDialog.show()
                }
            ),
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp
    ) {

        Column(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Row(Modifier.padding(4.dp)) {
                Text(
                    text = stringResource(R.string.name),
                    color = colorResource(id = R.color.black)
                )
                (Text(text = user.name, Modifier.padding(start = 8.dp)))
            }
            Row(Modifier.padding(4.dp)) {
                Text(
                    text = stringResource(R.string.mail_id),
                    color = colorResource(id = R.color.black)
                )
                (Text(text = user.email, Modifier.padding(start = 12.dp)))
            }
            Row(Modifier.padding(4.dp)) {
                Text(
                    text = stringResource(R.string.city),
                    color = colorResource(id = R.color.black)
                )
                (Text(text = user.address.city, Modifier.padding(start = 16.dp)))
            }
        }
    }
}



