package com.example.precticla41.viewModel


import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.precticla41.model.Users
import com.example.precticla41.service.ApiService

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    val _usersResponse = MutableStateFlow<List<Users>>(emptyList())
    val userResponse: StateFlow<List<Users>>
        get() = _usersResponse


    fun getUserList() {
        viewModelScope.launch {
            val apiService = ApiService.getInstance()
            try {
                val userList = apiService.getUserList()
                _usersResponse.value = userList

            } catch (e: Exception) {

            }

        }

    }

    fun removeItem(item: Users) { _usersResponse.value = _usersResponse.value.filter { it != item }.toList()
    }
}